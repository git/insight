/* Insight Definitions for GDB, the GNU debugger.
   Written by Keith Seitz <kseitz@sources.redhat.com>

   Copyright (C) 2003-2023 Free Software Foundation, Inc.

   This file is part of Insight.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

#include "defs.h"
#include "interps.h"
#include "target.h"
#include "ui-file.h"
#include "ui-out.h"
#include "cli-out.h"
#include <string.h>
#include "cli/cli-cmds.h"
#include "cli/cli-decode.h"
#include "exceptions.h"
#include "gdbsupport/event-loop.h"
#include "ui.h"

#include "tcl.h"
#include "tk.h"
#include "gdbtk.h"
#include "gdbtk-interp.h"

#ifdef __MINGW32__
# define WIN32_LEAN_AND_MEAN
# include <windows.h>
#endif


static void hack_disable_interpreter_exec (const char *, int);


/* See note in gdbtk_interp::init */
static void
hack_disable_interpreter_exec (const char *args, int from_tty)
{
  error ("interpreter-exec not available when running Insight");
}

static void
gdbtk_do_simple_func (const char *args, int from_tty,
                      struct cmd_list_element *c)
{
  c->function.simple_func (args, from_tty);
}

void
gdbtk_ui_out::do_progress_start ()
{
  m_count = 0;
  gdbtk_debuginfo_progress ("BEGIN", "");
}

void
gdbtk_ui_out::do_progress_notify (const std::string &msg, const char *unit,
				  double howmuch, double total)
{
  /* Restore total value. */
  if (unit)
    switch (*unit)
      {
      case 'G':
	total *= 1024.0;
	[[fallthrough]];
      case 'M':
	total *= 1024.0;
	[[fallthrough]];
      case 'K':
	total *= 1024.0;
	break;
    }

  gdbtk_debuginfo_progress ("MESSAGE", msg.c_str ());
  if (total < 0.0 || howmuch < 0.0)
    gdbtk_debuginfo_progress ("BUMP", std::to_string (++m_count).c_str ());
  else {
    gdbtk_debuginfo_progress ("TOTAL",
			      std::to_string ((unsigned long) total).c_str ());
    gdbtk_debuginfo_progress ("FRACTION", std::to_string (howmuch).c_str ());
  }
}

void
gdbtk_ui_out::do_progress_end ()
{
  gdbtk_debuginfo_progress ("DONE", "");
}


gdbtk_interp::gdbtk_interp (const char * name): interp (name)
{
  _stdout = NULL;
  _stderr = NULL;
  _stdlog = NULL;
  _stdlog = NULL;
  _stdtarg = NULL;
  _stdtargin = NULL;
  uiout = NULL;

  tcl = NULL;
}

gdbtk_interp::~gdbtk_interp()
{
 if (tcl)
   {
     Tcl_DeleteInterp (tcl);
     gdbtk_uninstall_notifier ();
     tcl = NULL;
   }
}

void
gdbtk_interp::init (bool top_level)
{
  /* Disable interpreter-exec. It causes us big trouble right now. */
  struct cmd_list_element *cmd = NULL;
  struct cmd_list_element *alias = NULL;
  struct cmd_list_element *prefix = NULL;

  _stdout = gdbtk_fileopen ();
  _stderr = gdbtk_fileopen ();
  _stdlog = gdbtk_fileopen ();
  _stdtarg = gdbtk_fileopen ();
  _stdtargin = gdbtk_fileopen ();
  uiout = new gdbtk_ui_out (_stdout, ui_source_list);

  gdbtk_init (this);

  if (lookup_cmd_composition ("interpreter-exec", &alias, &prefix, &cmd))
    {
      /* Change command processor function. */
      cmd->func = gdbtk_do_simple_func;
      cmd->function.simple_func = hack_disable_interpreter_exec;
    }
}

void
gdbtk_interp::resume ()
{
  static int started = 0;

  gdbtk_add_hooks ();

  gdb_stdout = _stdout;
  gdb_stderr = _stderr;
  gdb_stdlog = _stdlog;
  gdb_stdtarg = _stdtarg;
  gdb_stdtargin = _stdtargin;

  /* 2003-02-11 keiths: We cannot actually source our main Tcl file in
     our interpreter's init function because any errors that may
     get generated will go to the wrong gdb_stderr. Instead of hacking
     our interpreter init function to force gdb_stderr to our ui_file,
     we defer sourcing the startup file until now, when gdb is ready
     to let our interpreter run. */
  if (!started)
    {
      started = 1;
      gdbtk_source_start_file (this);
    }
}

void
gdbtk_interp::suspend ()
{
}

void
gdbtk_interp::exec (const char *command_str)
{
  /* Never called. */
}

/* This function is called before entering gdb's internal command loop.
   This is the last chance to do anything before entering the event loop. */

void
gdbtk_interp::pre_command_loop ()
{
  /* We no longer want to use stdin as the command input stream: disable
     events from stdin. */
  main_ui->input_fd = -1;

  if (Tcl_Eval (tcl, "gdbtk_tcl_preloop") != TCL_OK)
    {
      const char *msg;

      /* Force errorInfo to be set up propertly.  */
      Tcl_AddErrorInfo (tcl, "");

      msg = Tcl_GetVar (tcl, "errorInfo", TCL_GLOBAL_ONLY);
#ifdef _WIN32
      MessageBox (NULL, msg, NULL, MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
      gdb_puts (msg, gdb_stderr);
#endif
    }

#ifdef _WIN32
  close_bfds ();
#endif
}

ui_out *
gdbtk_interp::interp_ui_out ()
{
  return uiout;
}

void
gdbtk_interp::set_logging (ui_file_up logfile,
                           bool logging_redirect,
                           bool debug_redirect)
{
}

/* Called after a `set' command succeeds.  Runs the Tcl hook
   `gdb_set_hook' with the full name of the variable (a Tcl list) as
   the first argument and the new value as the second argument.  */

void
gdbtk_interp::on_param_changed (const char *param, const char *value)
{
  Tcl_DString cmd;

  Tcl_DStringInit (&cmd);
  Tcl_DStringAppendElement (&cmd, "gdbtk_tcl_set_variable");
  Tcl_DStringAppendElement (&cmd, param);
  Tcl_DStringAppendElement (&cmd, value);

  if (Tcl_Eval (tcl, Tcl_DStringValue (&cmd)) != TCL_OK)
    report_error ();

  Tcl_DStringFree (&cmd);
}

/*
 * This is run by the trace_find_command.  arg is the argument that was passed
 * to that command, from_tty is 1 if the command was run from a tty, 0 if it
 * was run from a script.  It runs gdbtk_tcl_tfind_hook passing on these two
 * arguments.
 *
 */

void
gdbtk_interp::on_traceframe_changed (int tfnum, int tpnum)
{
  Tcl_Obj *cmdObj = Tcl_NewListObj (0, NULL);

  Tcl_ListObjAppendElement (tcl, cmdObj,
			    Tcl_NewStringObj ("gdbtk_tcl_trace_find_hook", -1));
  Tcl_ListObjAppendElement (tcl, cmdObj, Tcl_NewIntObj (tfnum));
  Tcl_ListObjAppendElement (tcl, cmdObj, Tcl_NewIntObj (tpnum));
#if TCL_MAJOR_VERSION == 8 && (TCL_MINOR_VERSION < 1 || TCL_MINOR_VERSION > 2)
  if (Tcl_GlobalEvalObj (tcl, cmdObj) != TCL_OK)
    report_error ();
#else
  if (Tcl_EvalObj (tcl, cmdObj, TCL_EVAL_GLOBAL) != TCL_OK)
    report_error ();
#endif
}

/* Error-handling function for all hooks */
/* Hooks are not like tcl functions, they do not simply return */
/* TCL_OK or TCL_ERROR.  Also, the calling function typically */
/* doesn't care about errors in the hook functions.  Therefore */
/* after every hook function, report_error should be called. */
/* report_error can just call Tcl_BackgroundError() which will */
/* pop up a messagebox, or it can silently log the errors through */
/* the gdbtk dbug command.  */

void
gdbtk_interp::report_error (void)
{
  TclDebug ('E', Tcl_GetVar (tcl, "errorInfo", TCL_GLOBAL_ONLY));
  /*  Tcl_BackgroundError(tcl); */
}

/* Get insight's current interpreter. */
gdbtk_interp *
gdbtk_get_interp (void)
{
  if (!current_interp_named_p(INTERP_INSIGHT))
    throw_error (NOT_FOUND_ERROR, "current intepreter is not insight's");
  return (gdbtk_interp *) current_interpreter ();
}

/* Factory for GUI interpreter. */
static struct interp *
gdbtk_interp_factory (const char *name)
{
  return new gdbtk_interp (name);
}

void
initialize_gdbtk_interp (void)
{
  interp_factory_register (INTERP_INSIGHT, gdbtk_interp_factory);
}
